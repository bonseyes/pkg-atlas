/*
 * This file generated on line 754 of /home/miguel/bonseyes/wp4-lpdnn-sdk/ext/pkg-atlas/src//tune/blas/ger/r2hgen.c
 */
#ifndef ATLAS_DR2KERNELS_H
   #define ATLAS_DR2KERNELS_H

void ATL_dger2k__900002
   (ATL_CINT, ATL_CINT, const double*, const double*, const double*,
    const double*, double*, ATL_CINT);
void ATL_dger2k__900006
   (ATL_CINT, ATL_CINT, const double*, const double*, const double*,
    const double*, double*, ATL_CINT);
void ATL_dger2k__900008
   (ATL_CINT, ATL_CINT, const double*, const double*, const double*,
    const double*, double*, ATL_CINT);
void ATL_dger2k__900005
   (ATL_CINT, ATL_CINT, const double*, const double*, const double*,
    const double*, double*, ATL_CINT);
void ATL_dger2k__900002
   (ATL_CINT, ATL_CINT, const double*, const double*, const double*,
    const double*, double*, ATL_CINT);


#endif /* end guard around atlas_dr2kernels.h */

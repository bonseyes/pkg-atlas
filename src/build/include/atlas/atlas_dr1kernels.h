/*
 * This file generated on line 730 of /home/miguel/bonseyes/wp4-lpdnn-sdk/ext/pkg-atlas/src//tune/blas/ger/r1hgen.c
 */
#ifndef ATLAS_DR1KERNELS_H
   #define ATLAS_DR1KERNELS_H

void ATL_dgerk__900002
   (ATL_CINT, ATL_CINT, const double*, const double*, double*, ATL_CINT);
void ATL_dgerk__900009
   (ATL_CINT, ATL_CINT, const double*, const double*, double*, ATL_CINT);
void ATL_dgerk__900008
   (ATL_CINT, ATL_CINT, const double*, const double*, double*, ATL_CINT);
void ATL_dgerk__6
   (ATL_CINT, ATL_CINT, const double*, const double*, double*, ATL_CINT);
void ATL_dgerk__900006
   (ATL_CINT, ATL_CINT, const double*, const double*, double*, ATL_CINT);
void ATL_dgerk__900009
   (ATL_CINT, ATL_CINT, const double*, const double*, double*, ATL_CINT);
void ATL_dgerk__900005
   (ATL_CINT, ATL_CINT, const double*, const double*, double*, ATL_CINT);
void ATL_dgerk__900002
   (ATL_CINT, ATL_CINT, const double*, const double*, double*, ATL_CINT);


#endif /* end guard around atlas_dr1kernels.h */

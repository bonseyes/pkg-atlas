/*
 * This file generated on line 730 of /home/miguel/bonseyes/wp4-lpdnn-sdk/ext/pkg-atlas/src//tune/blas/ger/r1hgen.c
 */
#ifndef ATLAS_CR1KERNELS_H
   #define ATLAS_CR1KERNELS_H

void ATL_cgerk__900002
   (ATL_CINT, ATL_CINT, const float*, const float*, float*, ATL_CINT);
void ATL_cgerk__900008
   (ATL_CINT, ATL_CINT, const float*, const float*, float*, ATL_CINT);
void ATL_cgerk__900007
   (ATL_CINT, ATL_CINT, const float*, const float*, float*, ATL_CINT);
void ATL_cgerk__900008
   (ATL_CINT, ATL_CINT, const float*, const float*, float*, ATL_CINT);
void ATL_cgerk__900005
   (ATL_CINT, ATL_CINT, const float*, const float*, float*, ATL_CINT);
void ATL_cgerk__900002
   (ATL_CINT, ATL_CINT, const float*, const float*, float*, ATL_CINT);


#endif /* end guard around atlas_cr1kernels.h */

# ATLAS

if (ANDROID)
  return()
endif()

# Atlas through several tests and based on the statistics, selects the best configuration
# Pick the best configuration for the target processor.
if(NOT CMAKE_CROSSCOMPILING AND CMAKE_SYSTEM_PROCESSOR MATCHES "86")
  # Automatically select the target HW at build time.
  # Works only for x86_64 systems.
  ExternalProject_Add(atlas_project
  SOURCE_DIR ${CMAKE_CURRENT_LIST_DIR}/src      
  CONFIGURE_COMMAND ${CMAKE_CURRENT_LIST_DIR}/src/configure 
      -b 64
      --prefix=${CMAKE_BINARY_DIR}/deps 
      # The cripple flag here below is needed to be able to compile Atlas also on machines
      # where throttling is enabled. In this case we get a long warning message
      # at build time and performances will be very poor.
      --cripple-atlas-performance
  BUILD_COMMAND make build
  )
else()
  if(CMAKE_SYSTEM_PROCESSOR MATCHES "aarch64")
    set(ATLAS_PREBUILT_DIR "${CMAKE_SOURCE_DIR}/ext/prebuilt/linux/arm64/atlas")
  elseif(CMAKE_SYSTEM_PROCESSOR MATCHES "arm")
    set(ATLAS_PREBUILT_DIR "${CMAKE_SOURCE_DIR}/ext/prebuilt/linux/arm32/atlas")
  else()
    message(WARNING "Atlas prebuilt library not avaliable for target processor")
    return()
  endif()

  # Create lib directory to copy the files
  file(MAKE_DIRECTORY ${CMAKE_BINARY_DIR}/deps/lib)
  file(MAKE_DIRECTORY ${CMAKE_BINARY_DIR}/deps/include/atlas)
  configure_file("${ATLAS_PREBUILT_DIR}/lib/libatlas.a" "${CMAKE_BINARY_DIR}/deps/lib" COPYONLY)
  configure_file("${ATLAS_PREBUILT_DIR}/lib/libcblas.a" "${CMAKE_BINARY_DIR}/deps/lib" COPYONLY)
  configure_file("${ATLAS_PREBUILT_DIR}/include/cblas.h" "${CMAKE_BINARY_DIR}/deps/include/atlas" COPYONLY)
  # Create dummy project (referenced by pkg-cblas)
  add_custom_target(atlas_project)
endif()
